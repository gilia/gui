import {class_options_gui} from './class-options-gui.js'
import {namespace_gui} from './namespace-gui.js'


# Manage the main window, the navbar and main components.
#
# Creates subcomponents.
class MainGui

    constructor: () ->
        @namespace_gui = namespace_gui
        @class_options_gui = class_options_gui
    
    assign_events: () ->
        # TODO: Move this events to NavbarGui
        $("#btn_namespaces").on 'click', () ->
            namespace_gui.show()


export main_gui = new MainGui

$.when($.ready).then () ->
    main_gui.assign_events()
