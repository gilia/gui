# reasoning_editor.coffee --
# Copyright (C) 2019 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

exports = exports ? this
exports.reasoning_editor = exports.reasoning_editor ? {}

# Assign events to all buttons.
#
# @namespace reasoning_editor
reasoning_editor.assign_events = () ->
    $("#inference").on 'click', semcheck.check


$.when($.ready).then () ->
    reasoning_editor.assign_events()
    console.log 'reasoning_editor initialized'
