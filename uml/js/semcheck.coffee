# semcheck.coffee --
# Copyright (C) 2019 cnngimenez

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


exports = exports ? this
exports.semcheck = exports.semcheck ? {}

# Server
#
# Represents the server API for consistency checks.
#
# @namespace semcheck
class Server

    constructor: (url) ->
        @url = url
        @translate_suffix =
            berardi: '/reasoning/translate/berardi.php'
            crowd: '/reasoning/translate/crowd.php'
        @check_suffix = '/reasoning/querying/satisfiable.php'
        @answer_event = (answer) ->
            console.log answer

    # Send a diagram representation to the server.
    # 
    # @param diagram [object] A diagram representation.
    # @param conversion [string] A type of conversion to use.
    # @return [string] A JSON response from the server.
    request_check: (diagram, conversion="berardi") ->
        $.post @url + @check_suffix + '?json_version=2', {
            json: JSON.stringify diagram,
            },
            (result) =>
                @answer_event result

    # Request the translation of the diagram into OWL 2.
    # 
    # @param conversion [string] A type of conversion to use
    # @return [string] The OWL 2 (response from the server).
    request_translation: (diagram, conversion="berardi") ->
        $.post @url + @translate_suffix[conversion] + '?json_version=1',
            json: JSON.stringify(diagram)


exports.semcheck.prefix = 'http://crowd.fi.uncoma.edu.ar/kb1#'

exports.semcheck.iris2names = (iris) ->
    for curl in iris
        do (curl) ->
            curl.replace semcheck.prefix, ''    

# Process the server answer
exports.semcheck.process_answer = (result) ->
    json = JSON.parse result
    console.log json
    answer = json.answer

    satisf = semcheck.iris2names answer.satisfiable.classes
    insatisf = semcheck.iris2names answer.unsatisfiable.classes
    modelcheck.mark_all satisf, insatisf

# An instance of semcheck.Server
# 
# @namespace semcheck
exports.semcheck.iserver = new Server(config.server.url)


# Do the overall check.
#
# @namespace semcheck
exports.semcheck.check = () ->
    semcheck.iserver.answer_event = (answer) ->
        dialog = $("#modal-reasoner-output")
        codediv = dialog.find "code"
        codediv.html answer
        dialog.modal 'show'

        semcheck.process_answer answer

    semcheck.iserver.request_check generateJSON()
