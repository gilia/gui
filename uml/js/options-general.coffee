# options-general.coffee --
# Copyright (C) 2019 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

options = options ? {}


# Provides the General Options interface
#
# @mixin options.general
options.general =
    
    create_html: () ->
        div = $("<div>").attr 'class', 'menu-gral m-2'
        div.append $("<hr/>")
        
        hidediv = $("<div>").attr 'class', 'btn-group btn-group-sm'
        btn = $("<button>")
        btn.append $('<i>').attr
            class: 'fas fa-search-minus'
        btn.append ' Hide methods'
        btn.attr 'class', 'btn btn-secondary btn-hide-methods'
        hidediv.append btn
        btn = $("<button>")
        btn.append $('<i>').attr
            class: 'fas fa-search-plus'
        btn.append ' Show methods'
        btn.attr 'class', 'btn btn-secondary btn-show-methods'
        hidediv.append btn

        div.append hidediv

        btn = $("<button>")
        btn.append $('<i>').attr
            class: 'fas fa-broom'
        btn.append ' Clear diagram'
        btn.attr 'class', 'btn btn-danger btn-clear-all btn-sm'
        div.append btn

        div

    add_html: () ->
        $('#menuOpciones').append options.general.create_html()

    show: () ->
        $('.menu-gral').show()

    hide: () ->
        $('.menu-gral').hide()

    # Hide all methods and attributes from all the classes.
    hide_all_methods: () ->
        all_cells = graphMain.getCells()

        # These are elements from the palette
        all_cells = all_cells.concat paletteGraph.getCells()
        
        # Filter only the classes
        all_classes = all_cells.filter (elt) ->
            elt.attributes.type == 'uml.Class'

        # Hide methods and attributes for each class
        for cell in all_classes
            do (cell) ->
                cell.attr('.uml-class-methods-text/text','')
                cell.attr('.uml-class-attrs-text/text','')
                cell.attr('.uml-class-methods-rect').height = 0
                cell.attr('.uml-class-attrs-rect').height  = 0
                cell.attr('.uml-class-name-rect').height = 40
                cell.attr 'customAttr/isCompact', true
                view = paper.findViewByModel cell
                if not view?
                    view = palette.findViewByModel cell
                view.update()

    show_all_methods: () ->
        all_cells = graphMain.getCells()

        # These are elements from the palette
        all_cells = all_cells.concat paletteGraph.getCells()

        # Filter only the classes
        all_classes = all_cells.filter (elt) ->
            elt.attributes.type == 'uml.Class'

        # Hide methods and attributes for each class
        for cell in all_classes
            do (cell) ->
                cell.attr('.uml-class-methods-text/text','')
                cell.attr('.uml-class-attrs-text/text','')
                cell.attr('.uml-class-methods-rect').height = 20
                cell.attr('.uml-class-attrs-rect').height  = 20
                cell.attr('.uml-class-name-rect').height = 40
                cell.attr 'customAttr/isCompact', false
                view = paper.findViewByModel cell
                if not view?
                    view = palette.findViewByModel cell
                view.update()

    clear_all: () ->
        result = window.confirm 'Are you sure you want to clear all the diagram?'

        if result
            graphMain.clear()

    assign_events: () ->
        $('.btn-hide-methods').on 'click', () ->
            options.general.hide_all_methods()
        $('.btn-show-methods').on 'click', () ->
            options.general.show_all_methods()
        $('.btn-clear-all').on 'click', () ->
            options.general.clear_all()

export initialize = () ->
    options.general.add_html()
    options.general.assign_events()
