/*
Este archivo contiene las funcionalidades del diagrama uml y mecanicas del paper principal.
- Definicion y creacion de links.
- Funcionalidad para drag and drop de la paleta.
- Eventos para mostrar los menus de opciones de los componentes del diagrama.
- Eventos para mostrar y ocultar el toolbar de los componentes del diagrama.
- Seleccion de super-class para la generalizacion.
- Exportar JSON
- Check semantico.
*/

var main_gui=null;
import("./main-gui.js").then(function(module){
    main_gui = module.main_gui;
    console.log("main-gui loaded");
});

//graph para el diagrama uml
var graphMain = new joint.dia.Graph();
//paper para manejar el graph diagrama class
var paper = new joint.dia.Paper({
    el: document.getElementById('paper'),
    width: $('#paper').width(),
    height: $('#paper').height(),
    model: graphMain,
    gridSize: 10,
    snapLinks: { radius: 75 },
    linkPinning: false,
    markAvailable: true,
    defaultLink: createLinkProperty2Property(),
    interactive: { vertexAdd: true },
    validateConnection: function (cellViewS, magnetS, cellViewT, magnetT, end, linkView) {
        //solo usado para conectar ports. los otros enlaces se verifican manualmente.
        //solo se pueden conectar ports con ports. No se puede conectar un port con si mismo
        if (magnetT && magnetT.getAttribute('port-group') === 'link2link' && cellViewS!=cellViewT) {
            linkView.addTools(addToolsToDefaultLink());
            return true;
        }
        return false;
    },
});

///////////////////////////////////////////////////
///////Eventos referidos a herramientas_visuales.js
///////////////////////////////////////////////////
//para mostrar las herramientas cuando se hace click sobre un elemento
paper.on('element:pointerclick', function (cellView, evt) {
    actualElement = cellView;
    if (getType(cellView.model)=='Class'){
        showElementToolsClass(cellView);
    }else if (getType(cellView.model)=='Inheritance'){
        showElementToolsGeneralization(cellView);
    }else if (getType(cellView.model)=='Nary'){
        showElementToolsNary(cellView);
    }
});

/*para mostrar el menu de opciones cuando se hace doble click sobre una cell.*/
paper.on('cell:pointerdblclick', function (cellView, evt) {
    //eliminar cualquier menu abierto
    ocultarMenues();
    if (getType(cellView.model) == 'Class'){
        main_gui.class_options_gui.set_cell(cellView.model);
        main_gui.class_options_gui.show();
        // menuClass(cellView);
    }else if (getType(cellView.model) == 'Inheritance'){
        menuGeneralization(cellView);
    }else if (getType(cellView.model) == 'Link'){
        menuLink(cellView);
    }
});


//muestra las herramientas para las generalizaciones
function showElementToolsGeneralization(figure) {
    var pos = paper.localToClientPoint(figure.model.attributes.position);
    //var screenPos = paper.localToClientPoint(pos);
    tools.width($("#"+figure.model.id).width());
    tools.height($("#"+figure.model.id).height());
    /*tools.width(figure.model.attributes.size.width * paper.scale().sx + $('.tools').width() * 2);
    tools.height(figure.model.attributes.size.height * paper.scale().sy + $('.tools').height() * 2);*/
    tools.attr('elementid', figure.model.id);
    tools.css({
        top: pos.y - 25,
        left: pos.x - 20,
        display: 'block'
    });

    //oculta/muestra la opcion de seleccionar super entity
    $('#selectSuperEntity').css({
        display: 'block'
    });   

    $('#compactClass').css({
        display: "none"
    });
}

//recibe una uml.class para mostrar el elementTools
function showElementToolsClass(cls) {
    var pos = paper.localToClientPoint(cls.model.attributes.position);
    //var screenPos = paper.localToClientPoint(pos);
    tools.width($("#"+cls.id).width()+45);
    tools.height($("#"+cls.id).height()+20);
    tools.attr('elementid', cls.model.id);
    tools.css({
        top: pos.y-20,
        left: pos.x-20,
        display: 'block'
    });

    $('#compactClass').css({
        display: "block"
    });
    //oculta la opcion de seleccionar super entity
    $('#selectSuperEntity').css({
        display: 'none'
    });
}
//muestra las herramientas de Nary (rombo). (solo borrar element)
function showElementToolsNary(cls) {
    var pos = paper.localToClientPoint(cls.model.attributes.position);
    //var screenPos = paper.localToClientPoint(pos);
    tools.width($("#"+cls.id).width()+45);
    tools.height($("#"+cls.id).height()+20);
    tools.attr('elementid', cls.model.id);
    tools.css({
        top: pos.y-20,
        left: pos.x-20,
        display: 'block'
    });

    $('#compactClass').css({
        display: "none"
    });
    //oculta la opcion de seleccionar super entity
    $('#selectSuperEntity').css({
        display: 'none'
    });
}

paper.on('element:pointermove', function (cellView, evt) {
    hideElementTools();
});

$('#paper').on('mousewheel DOMMouseScroll', function (evt) {
    hideElementTools();
});

/*para pedir confirmacion al eliminar un elemento*/
/*paper.on('element:delete', function (elementView, evt) {
evt.stopPropagation();
if (confirm('Are you sure you want to delete this element?')) {
elementView.model.remove();
}
});*/


//////////////////////////////////////////////////////
////////////////////////////////FUNCIONES DE LA PALETA
//////////////////////////////////////////////////////

/**
Copy the attributes refering to size.

It is used to copy the size of the collapsable parts of the view
when the original class is collapsed. Take note that when clonning the
palette class, its attr elements won't clone.

@param newClass {Cell} A JointJS model.
@param protclass {Cell} A JointJS model.
*/
function copy_class_attrs(newClass, protclass){
    newClass.attr('data-class-url',
                  protclass.attr('data-class-url'));
    newClass.attr('.uml-class-methods-text/text',
                  protclass.attr('.uml-class-methods-text/text'));
    newClass.attr('.uml-class-attrs-text/text',
                  protclass.attr('.uml-class-attrs-text/text'));
    newClass.attr('.uml-class-methods-rect',
                  protclass.attr('.uml-class-methods-rect'));
    newClass.attr('.uml-class-attrs-rect',
                  protclass.attr('.uml-class-attrs-rect'));
    newClass.attr('.uml-class-name-rect',
                  protclass.attr('.uml-class-name-rect'));
    newClass.attr('customAttr/isCompact',
                  protclass.attr('customAttr/isCompact'));
}

//para el drag and drop de la paleta
palette.on('cell:pointerdown', function (cellView, e, x, y) {
    $('body').append('<div id="flyPaper"></div>');
    var flyGraph = new joint.dia.Graph;
    var flyPaper = new joint.dia.Paper({
            el: $('#flyPaper'),
            model: flyGraph,
            interactive: false,
    });
    var flyShape = cellView.model.clone();
    var pos = cellView.model.position();
    var offset = {
        x: flyShape.attributes.size.width / 2 * paper.scale().sx,
        y: flyShape.attributes.size.height / 2 * paper.scale().sy
    };
    
    flyPaper.scale(paper.scale().sx, paper.scale().sy);
    flyShape.position(0, 0);

    flyGraph.addCell(flyShape);
    if (cellView.model.attributes.type == 'uml.Class'){
        copy_class_attrs(flyShape, cellView.model);
        flyPaper.findViewByModel(flyShape).update();
    }

    $("#flyPaper").offset({
        left: (e.pageX - offset.x),
        top: (e.pageY - offset.y)
    });
    
    $('body').on('mousemove.fly', function (e) {
        $("#flyPaper").offset({
            left: (e.pageX - offset.x),
            top: (e.pageY - offset.y)
        });
    });
    
    $('body').on('mouseup.fly', function (e) {
        var x = e.pageX,
            y = e.pageY,
            target = paper.$el.offset();
        origin = palette.$el.offset();
        // Dropped over paper and not over origin
        if ((x > target.left && x < target.left + paper.$el.width() &&
             y > target.top && y < target.top + paper.$el.height()) &&
            !(x > origin.left && x < origin.left + palette.$el.width() &&
              y > origin.top && y < origin.top + palette.$el.height())) {
            var newElement = flyShape.clone();
            var relativePoint = paper.clientToLocalPoint(e.clientX, e.clientY);
            /*var localRect1 = paper.clientToLocalRect(target.left,target.top,target.width,target.height);
             newElement.position(((x - target.left - offset.x)+localRect1.center().x), ((y - target.top - offset.y)+localRect1.center().y));*/
            newElement.position(relativePoint.x -
                                (newElement.attributes.size.width / 2),
                                relativePoint.y -
                                (newElement.attributes.size.height / 2));

            if (getType(cellView.model)!='Nary'){
                graphMain.addCell(newElement);

                if (newElement.attributes.type == 'uml.Class'){
                    copy_class_attrs(newElement, flyShape);
                    paper.findViewByModel(newElement).update();
                }

            }else{
            //si se selecciona una relacion Nary, crear la relacion binaria con clase.
            dragNaryToGraph(newElement, relativePoint);
        }
    }
    $('body').off('mousemove.fly').off('mouseup.fly');
    flyShape.remove();
    $('#flyPaper').remove();
});
});

//Cuando se usa la relacion con clase (el rombo), se crean las clases y las relaciones con el rombo.
function dragNaryToGraph(rombo, point){
    var classAux = getElementFromPalette("Class");
    //agregar los elementos al graph
    graphMain.addCell(rombo);
    var clsA = classAux.clone();
    clsA.position(point.x - 220, (point.y - (rombo.attributes.size.height / 2))-20);
    var clsB = classAux.clone();
    clsB.position(point.x + 120, (point.y - (rombo.attributes.size.height / 2))-20);
    var clsC = classAux.clone();
    clsC.position(point.x-50, (point.y - (rombo.attributes.size.height / 2))+90);
    graphMain.addCell(clsA);
    graphMain.addCell(clsB);
    graphMain.addCell(clsC);
    var newLinkA = createLinkClass('class2rombo');
    connectLink(newLinkA, clsA, rombo);
    setAttrsLinkClass2Rombo(newLinkA, 1, '1..6', 'RoleA');
    var newLinkB = createLinkClass('class2rombo');
    connectLink(newLinkB, clsB, rombo);
    setAttrsLinkClass2Rombo(newLinkB, -1,  '1..6', 'RoleB');
    var newLinkC = createLinkClassDashed();
    connectLink(newLinkC, clsC, rombo);
    addPortToLink(newLinkA);
    addPortToLink(newLinkB);
}

//retorna el elemento uml.Class de la paleta
function getElementFromPalette(elem) {
    //buscar el elemento uml.Class de la paleta
    var classAux = null;
    for (var i = paletteElements.length - 1; i >= 0; i--) {
        if (getType(paletteElements[i])==elem){
            classAux = paletteElements[i];
            break;
        }
    }
    return classAux;
}


///////////////////////////////////////////////////////////////////////////////
/////Metodos auxiliares para facilitar las comparaciones de nombres de elementos
//////////////////////////////////////////////////////////////////////////////
function getSpecificType(elementModel) {
    var type = elementModel.attributes.type;
    var resultType;
    if (type === 'uml.Class') {
        resultType = 'Class';
    } else if (type === 'uml.Association') {
        resultType = 'Association';
    } else if (type === 'erd.Relationship') {
        resultType = 'Nary';
    } else if (type === 'erd.Inheritance') {
        resultType = 'Inheritance';
    } else if (type === 'standard.Link'){
        resultType = 'Link';
    } else if (type === 'tydevs.Model'){
        resultType = 'Model';
    } else {
        resultType = 'Error';
    }
    return resultType;
}

function getType(elementModel) {
    var type = elementModel.attributes.type;
    var resultType;
    if (type === 'uml.Class') {
        resultType = 'Class';
    } else if (type === 'uml.Association') {
        resultType = 'Association';
    } else if (type === 'erd.Inheritance') {
        resultType = 'Inheritance';
    } else if (type === 'erd.Relationship') {
        resultType = 'Nary';
    } else if (type === 'standard.Link'){
        resultType = 'Link';
    } else if (type === 'devs.Model'){
        resultType = 'Model';
    } else {
        resultType = 'Error';
    }
    return resultType;
}

//////////////////////////////////
//////FUNCIONALIDAD GENERALIZACION
//////////////////////////////////

//para seleccionar la superClass en la generalizacion 
var ISASelected = null;
function selectSuperEntity(event) {
    ISASelected = actualElement.model;
    var links = graphMain.getConnectedLinks(ISASelected);
    for (var i = 0; i < links.length; i++) {
        var elm = (links[i].source().id != ISASelected.id ? graphMain.getCell(links[i].source().id) : graphMain.getCell(links[i].target().id));
        if (getType(elm) == 'Class') {
            markElement(elm,'selectGeneralizationType');
        }
    }
}

//setear la superClass de la herencia y agregar las subClasses y superClasses como atributos
paper.on('cell:pointerdown blank:pointerdown', function (elementView, evt) {
    var linkToSuperClass = null;
    if (ISASelected != null) {
        //agregar elementView al atributo superClasses de ISASelected
        ISASelected.attr('customAttr/superClasses',[elementView.model]);
        var links = graphMain.getConnectedLinks(ISASelected);
        var newSubClasses = [];
        //saca la marca a las entidades y verifica que se seleccionó alguna de ellas
        for (var i = 0; i < links.length; i++) {
            var link = links[i];
            var elm = (link.source().id != ISASelected.id ? graphMain.getCell(link.source().id) : graphMain.getCell(link.target().id));
            if (getType(elm) == 'Class') {
                unmarkElement(elm);
                if (elementView.model != null && elm.id == elementView.model.id) {
                    linkToSuperClass = link;
                }
                if (elementView.model != null && elementView.model.id!=elm.id && ISASelected.id!=elm.id){
                    //agregar elm al atributo subClasses de ISASelected
                    newSubClasses.push(elm);
                }
            }
        }

        ISASelected.attributes.attrs.customAttr.subClasses = newSubClasses;
        //vuelve a recorrer los link para marcar direction los links
        if (linkToSuperClass != null) {
            for (var i = 0; i < links.length; i++) {
                var link = links[i];
                setDirection(link,true);
                if (link == linkToSuperClass) {
                    setInheritance(link,true);
                } else {
                    setInheritance(link,false);
                }
            }
        }
        ISASelected = null;
        hideElementTools();
    }
});

//setea la direccion del link (util en el diagrama e-r)
function setDirection(link,value) {
    var marker = getType(graphMain.getCell(link.source().id)) == 'Class' ? 'sourceMarker' : 'targetMarker';
    link.attr('customAttr/direction',value);
}

//setea el attr inheritance y cambia el formato de las flechas
function setInheritance(link,value) {
    var marker = getType(graphMain.getCell(link.source().id)) == 'Class' ? 'sourceMarker' : 'targetMarker';
    link.attr('customAttr/inheritance',value);
    if (value) {
        //link.attr('line/'+marker+'/d','M 10 -5 0 0 10 5 Z');
        link.attr('line/'+marker+'/d','m 0 0 T 30 -15 q 0 0 0 30 z');
        link.attr('line/'+marker+'/type','path');
        link.attr('line/'+marker+'/stroke-width',2);
        link.attr('line/'+marker+'/fill','white');
    } else {
        link.attr('line/'+marker+'/d','');
    }
}


///////////////////////////////
////////MECANICAS SOBRE EL PAPER
///////////////////////////////

// para crear links arrastrando un elemento sobre otro
var dataElement;
paper.on({
	'element:pointerdown': function (elementView, evt) {
		evt.data = elementView.model.position();
		dataElement = evt;
	},

	'element:pointerup': function (elementView, evt, x, y) {
		var coordinates = new g.Point(x, y);
		var elementAbove = elementView.model;
		var elementBelow = this.model.findModelsFromPoint(coordinates).find(function (el) {
            return (el.id !== elementAbove.id);
        });

		// If the two elements are connected already, don't
		// connect them again (this is application-specific though).
		if (elementBelow && graphMain.getNeighbors(elementBelow).indexOf(elementAbove) === -1 && canConnect(elementBelow, elementAbove)) {

			// Move the element to the position before dragging.
			elementAbove.position(dataElement.data.x, dataElement.data.y);

			// Create a connection between elements.
			// var link = new joint.shapes.standard.Link();
			// link.source(elementAbove);
			// link.target(elementBelow);
			// link.addTo(graphMain);
			if (getType(elementBelow) != 'Attribute' && getType(elementAbove) != 'Attribute' &&
				getType(elementBelow) != 'Generalization' && getType(elementAbove) != 'Generalization') {
				//si es una relacion entre 2 clases, se agregan los labels para la cardinalidad
            if (getType(elementBelow)=='Class' && getType(elementAbove) == 'Class'){
             var newLink = createLinkClass('binaryAssociation');
             connectLink(newLink, elementBelow, elementAbove);
                setAttrsLinkClass2Class(newLink, '1..6', '1..5',
                                        'RoleOrigen', 'RoleDestino',
                                        'aName');
               //agregar port al link
               addPortToLink(newLink);
           }else if ((getType(elementBelow)=='Nary' && getType(elementAbove) == 'Class') 
            || (getType(elementBelow)=='Class' && getType(elementAbove) == 'Nary')){
                //el offset y el orden de los parametros en connectLink es solo para que los labels aparezcan del lado de la clase.
                var offset = (getType(elementBelow)=='Nary') ? -1 : -1;
                var newLinkA = createLinkClass('class2rombo');
                if ((getType(elementBelow)=='Nary')){
                    connectLink(newLinkA, elementAbove, elementBelow);
                }else{
                    connectLink(newLinkA, elementBelow, elementAbove);
                }
                setAttrsLinkClass2Rombo(newLinkA, offset);
                addPortToLink(newLinkA);
            }else{
                var newLink = createLink();
                connectLink(newLink, elementBelow, elementAbove);
            }
        }
    }else{
        if (elementBelow != undefined){           
                        // Move the element to the position before dragging.
                        elementAbove.position(dataElement.data.x, dataElement.data.y);
                    }
                }
            }
        });


/*devuelve true si se puede conectar dos elementos. 
Se pueden  conectar clases con clases, clases con herencia,
clases con relaciones Nary.*/
function canConnect(cellViewS, cellViewT) {
    var canConnect = false;
    if (cellViewS != null && cellViewT != null) {
        var sourceType = getType(cellViewS);
        var targetType = getType(cellViewT);
        var specificSourceType = getSpecificType(cellViewS);
        var specificTargetType = getSpecificType(cellViewT);
        //var linksS = getElementLinks(cellViewS);
        //var linksT = getElementLinks(cellViewT);
        //console.log(linksS);
        //console.log(linksT);
        if (sourceType === 'Class' && targetType === 'Class') {
            canConnect = true;
        } else if ((sourceType === 'Inheritance' && targetType === 'Class')||(sourceType=== 'Class' && targetType === 'Inheritance')) {
            canConnect = true;
        }else if ((sourceType === 'Nary' && targetType === 'Class')||( sourceType=== 'Class' && targetType === 'Nary')){
            canConnect = true;
        }
    } 
    return canConnect;
}


//para dragear la pagina
paper.on('blank:pointerdown', function (event, x, y) {
    hideElementTools();
	//dragStartPositionMain = { x: x, y: y};
	var scale = paper.scale();
	var dragStartPositionMain = {
		x: x * scale.sx,
		y: y * scale.sy
	};

    $("#paper").mousemove(function (event) {
        if (dragStartPositionMain != null) {
        //console.log("mousemove");
        paper.translate(
            event.offsetX - dragStartPositionMain.x,
            event.offsetY - dragStartPositionMain.y);
    }
});

    paper.on('cell:pointerup blank:pointerup', function (cellView, x, y) {
        dragStartPositionMain = null;
    });
});

paper.on('blank:mouseover', function() {
    paper.hideTools();
});

$('#paper').on('mousewheel DOMMouseScroll', function (evt, x, y) {
	evt.preventDefault();
	var delta = Math.max(-1, Math.min(1, (evt.originalEvent.wheelDelta || -evt.originalEvent.detail)));
	var newScale = paper.scale().sx + delta / 50;
    if (newScale > 0.4 && newScale < 2) {
		//paper.translate(0, 0);
		paper.scale(newScale, newScale); //, p.x, p.y);
	}
});





///////////////////////////////
/////////DEFINICIONES DE LINKS
///////////////////////////////

//crea un conector especifico (linea punteada) para conectar dos roles con links. 
//Este link no se agrega al graph automaticamente, porque se usa como defaultLink en la definición de maingraph.
function createLinkProperty2Property() {
    var myLink = new joint.shapes.standard.Link();

    myLink.attr({
        line: {
            stroke: 'gray',
            strokeWidth: 2,
            strokeDasharray: '5 5',
            sourceMarker: {},
            targetMarker: {
                'd': 'M 15 -5 L 15 5 L 0 0 z'
            }
        },
        customAttr:{
            type: 'link2port'
        }
    });

    return myLink;
}

//crea un conector generico. (Usado con superclase y subclases con el circulo de herencia)
var createLink = function () {
    var myLink = new joint.shapes.standard.Link();

    myLink.attr({
        line: {
            stroke: 'black',
            strokeWidth: 2,
            sourceMarker: {},
            targetMarker: {
                'd': ''
            }
        },
        customAttr: {
            total: false,
            direction: false,
            inheritance: false,
            type: 'generic'
        }
    });

    var link = myLink.addTo(graphMain);
    //para agregar controles en conectores
    var verticesTool = new joint.linkTools.Vertices();
    var segmentsTool = new joint.linkTools.Segments();
    var removeButton = new joint.linkTools.Remove({
       distance: 20
   });

    var toolsView = new joint.dia.ToolsView({
        tools: [verticesTool, segmentsTool, removeButton]
    });

    var linkView = myLink.findView(paper);
    linkView.addTools(toolsView);

    return link;
};

//crea un conector especifico para las relaciones binarias entre clases
var createLinkClass = function (type, cid=null) {
    var myLink = new joint.shapes.standard.Link();
    var default_iri = "http://crowd.fi.uncoma.edu.ar/crowd2/ontology/";
    myLink.prop('source', { x: 100, y: 100 });
    myLink.prop('target', { x: 200, y: 200 });
    if (cid != null) {
        myLink.cid = cid;
    }

    myLink.attr({
        connector: { name: 'smooth' },
        line: {
            stroke: 'black',
            strokeWidth: 2,
            sourceMarker: {},
            targetMarker: {
                'd': ''
            },
            vertexMarker: {
                'type': 'circle',
                'r': 5,
                'stroke-width': 2,
                'fill': 'black'
            }
        },
        customAttr: {
            total: false,
            direction: false,
            inheritance: false,
            type: type,
        },
    });


    var link = myLink.addTo(graphMain);
    
    link.attr('data-role-target-iri', default_iri);
    link.attr('data-role-origin-iri', default_iri);
    link.attr('data-assoc-iri', default_iri);

    //para agregar controles en conectores
    var removeButton = new joint.linkTools.Remove({
     distance: 20
 });
    var toolsView = new joint.dia.ToolsView({
     tools: [
     new joint.linkTools.Vertices({
        snapRadius: 0,
        focusOpacity: 0.5,
        snapRadius: 5,
        redundancyRemoval: false,
        vertexAdding: false,
        mouseenter: function(){
            //se sobreescribe esta funcion para poder ejecutar eventos de mouse sobre el port. 
            //El evento para crear vertices se crea manualmente con link:pointerclick
        }
    }),
     removeButton,
     ]
 });

    var linkView = myLink.findView(paper);
    linkView.addTools(toolsView);
    linkView.hideTools();
    return link;
};

//crea un conector especifico (linea punteada) para conectar un rombo con su clase.
var createLinkClassDashed = function () {
    var myLink = new joint.shapes.standard.Link();

    myLink.attr({
        line: {
            stroke: 'black',
            strokeWidth: 2,
            strokeDasharray: '5 5',
            sourceMarker: {},
            targetMarker: {
                'd': ''
            }
        },
        customAttr: {
            total: false,
            direction: false,
            inheritance: false,
            type: 'classAssociation'
        }
    });

    myLink.connector('jumpover', {
        size: 10
    });

    var link = myLink.addTo(graphMain);

        //para agregar controles en conectores
        var verticesTool = new joint.linkTools.Vertices({
            focusOpacity: 0.5,
            snapRadius: 5,
        });
        var segmentsTool = new joint.linkTools.Segments();
        var removeButton = new joint.linkTools.Remove({
            distance: 20
        });

        var toolsView = new joint.dia.ToolsView({
            tools: [verticesTool, segmentsTool, removeButton]
        });

        var linkView = myLink.findView(paper);
        linkView.addTools(toolsView);
        linkView.hideTools();

        return link;
    };


/////////////////////////////////
/////////FUNCIONALIDADES DE LINKS
/////////////////////////////////

//Retorna un toolsView (usado en validateConnection para links entre roles).
function addToolsToDefaultLink(){
    var verticesTool = new joint.linkTools.Vertices({
        focusOpacity: 0.5,
        snapRadius: 5,
    });
    var removeButton = new joint.linkTools.Remove({
        distance: 20,
    });

    var toolsView = new joint.dia.ToolsView({
        tools: [verticesTool, removeButton]
    });
    return toolsView;
}

paper.on('link:mouseenter', function (linkView, evt) {
    //console.log(linkView.sourcePoint);
    //para que no molesten las opciones de los links cuando se selecciona superEntity
    if (ISASelected == null) {
        linkView.showTools();
    }
    //hacemos visible el port del link
    if (linkView.model.attributes.attrs != undefined && linkView.model.attributes.attrs.customAttr!=undefined){
        if (linkView.model.attributes.attrs.customAttr.port != undefined){
            linkView.model.attributes.attrs.customAttr.port.attr('./display', 'block');
        }
    }

});

paper.on('link:pointerclick', function (linkView, evt, x, y) {
    linkView.addVertex(x, y);
});

paper.on('link:mouseleave', function(linkView, evt) {
    linkView.hideTools();
    if (linkView.model.attributes.attrs != undefined && linkView.model.attributes.attrs.customAttr!=undefined){
        if (linkView.model.attributes.attrs.customAttr.port != undefined){
            var relativePoint = paper.clientToLocalPoint(evt.clientX, evt.clientY);
            if (!linkView.model.attributes.attrs.customAttr.port.getBBox().containsPoint(relativePoint)){
                linkView.model.attributes.attrs.customAttr.port.attr('./display', 'none');
            }
        }
    }
});

var connectLink = function (myLink, elm1, elm2) {
    myLink.source({
        id: elm1.id
    });
    myLink.target({
        id: elm2.id
    });
};

//retorna un label con txt y position.
var createLabelWithPosition = function (txt, position) {
    var dis = position.distance;
    var off = position.offset;
    var label = {
        attrs: {
            text: {
                text: txt,
                fill: 'black',
                fontSize: 12
            },
            rect: {
                fill: 'white'
            }
        },
        position: {
            distance: dis,
            offset: off
        }
    }
    return label;
};


//agrega los labels y el name a un link entre dos clases
function setAttrsLinkClass2Class(link, CO, CD, RO, RD, name){
    var positionLabelCardOrigin = {
        distance: 0.85,
        offset: 15
    }
    var positionLabelCardDestino = {
        distance: 0.15,
        offset: 15
    }                  
    var positionLabelRoleOrigin = {
        distance: 0.70,
        offset: -15
    }
    var positionLabelRoleDestino = {
        distance: 0.30,
        offset: -15
    }
    var positionLabelName = {
        distance: 0.50,
        offset: 0
    }
    link.appendLabel(createLabelWithPosition(CO, positionLabelCardOrigin));
    link.appendLabel(createLabelWithPosition(CD, positionLabelCardDestino));
    link.appendLabel(createLabelWithPosition(RO, positionLabelRoleOrigin));
    link.appendLabel(createLabelWithPosition(RD, positionLabelRoleDestino));
    link.appendLabel(createLabelWithPosition(name, positionLabelName));
}

//agrega los labels a un link entre una clase y un rombo.
// off es 1 o -1, para acomodar los labels simetricamente
function setAttrsLinkClass2Rombo(link, off, CO, RO){
    var noffC = 15;
    var noffR = -15;
    if (off==-1){
        noffC = -15;
        noffR = 15;
    }
    var positionLabelCardOrigin = {
        distance: 0.10,
        offset: noffC
    }              
    var positionLabelRoleOrigin = {
        distance: 0.35,
        offset: noffR
    }
    link.appendLabel(createLabelWithPosition(CO, positionLabelCardOrigin));
    link.appendLabel(createLabelWithPosition(RO, positionLabelRoleOrigin));
}

////////////////////////////////////
/////////////PORT PARA LINKEAR ROLES
////////////////////////////////////

function addPortToLink(link){
    /*Port para hacer subClass link*/
    /*Se crea en la posicion 0,0, pero cuando se 
    renderiza el link se actualiza automaticamente 
    la posicion del port con la funcion updatePortPosition
    en la libreria jointjs*/

    var rect = new joint.shapes.basic.Rect({
        position: { x: 0, y: 0 },
        attrs: {
            rect: {
                fill: 'none',
                'ref-x': -12.5,
                stroke: 'none'
            },
            root: { magnet: false },
            customAttr: {
                embedLink: link
            }
        },
        size: { width: 30, height: 30 },
        ports: {
            groups: {
                'link2link': {
                    attrs: {
                        circle: {
                            fill: 'white',
                            magnet: true,
                            r: 7,
                            class: "circlePort",
                        },
                    },
                },
            },
            items:[ { group: 'link2link' }],
        }
    });
    graphMain.addCell(rect);
    //por defecto el rect que contiene al port es invisible
    rect.attr('./display', 'none');
    //se agrega este port como atributo del link para poder reconocer las relaciones de este link con otros links (jerarquia de links).
    link.attr('customAttr/port',rect);
    //embeber port en link. Asi cuando se elimina el link se elimina el port.
    link.embed(rect);
    rect.toFront();

}

//ocultar el port cuando el mouse sale desde el rectangulo del port.
paper.on('cell:mouseleave', function(cellView, evt) {
    if (cellView.model.getParentCell()!=null){
        if (getType(cellView.model.getParentCell())=='Link') {
            var linkModel = cellView.model.getParentCell();
            if (linkModel.attributes.attrs != undefined && linkModel.attributes.attrs.customAttr!=undefined){
                if (linkModel.attributes.attrs.customAttr.port != undefined){
                    linkModel.attributes.attrs.customAttr.port.attr('./display', 'none');
                }
            }        
        }
    }
});



////////////////////////////
/////////////////EXPORT JSON
////////////////////////////

/**
 Handler para el botón "Exportar"
*/
function exportJSON(){
}

/**
 Generate the JSON object from the diagram.

 Create a JSON object representation of the UML diagram designed by the user.
 Use `JSON.stringify(generateJSON())` to convert the JSON object into string.

 @return [object] The JSON representation of the UML diagram.
*/
function generateJSON() {
    var allElement = getAllElement();
    var classes = allElement[0];
    var inheritances = allElement[1];
    var assocWithClass = allElement[2];
    var associations = [];
    var elements = graphMain.getElements();
    var links = graphMain.getLinks();
    for (var i = 0; i < links.length; i++) {
        var link = links[i];
        if (link.attr('customAttr/type')=='binaryAssociation' ||
            link.attr('customAttr/type')=='class2rombo'){
            var element1 = graphMain.getCell(link.source().id);
            var element2 = graphMain.getCell(link.target().id);
            var infoLink = getInfoLink(link);
            var newLink = {
                info: infoLink,
                source: element1.cid,
                target: element2.cid,
                id: link.cid,
                type: link.attr('customAttr/type')
            }
            associations.push(newLink);
        }else if (link.attr('customAttr/type')=='link2port'){
            var sourcePortId = link.source().id;
            var targetPortId = link.target().id;
            var sourceP = graphMain.getCell(sourcePortId);
            var targetP = graphMain.getCell(targetPortId);
            var superLink = sourceP.attr('customAttr/embedLink');
            var subLink = targetP.attr('customAttr/embedLink');
            var newLink = {
                source: superLink.cid,
                target: subLink.cid,
                type: 'inheritanceRole'
            }
            associations.push(newLink);
        }
    }
    var json = {
        classes: classes,
        inheritances: inheritances,
        associationWithClass: assocWithClass,
        associations: associations
    }
    // console.log(JSON.stringify(json));
    return json;
}

function getAllElement() {
    /*retorna todos los elementos en un arreglo con la forma [entidades,relaciones,atributos,herencias,conectores]*/
    var inheritances = [];
    var classes = [];
    var assocWithClass = [];
    var elements = graphMain.getElements();

    for (var i = 0; i < elements.length; i++) {
        var element = elements[i];
        var type = getSpecificType(element);
        var name = element.attr('text/text');
        var cid = element.cid;
        var dataType = element.attr('customAttr/type');
        switch (type) {
            case "Class":
            name = element.attr('.uml-class-name-text/text');
            var newClass = {
                name: name,
                id: cid,
                attributes: element.attributes.attributes,
                methods: element.attributes.methods,
                position: element.position(),
                size: element.size()
            }
            classes.push(newClass);
            break;
            case "Nary":
            var clscid = getClassAssociation(element);
            var newAssoc = {
                id: cid,
                classAssociation: clscid,
                position: element.position(),
                size: element.size()
            }
            assocWithClass.push(newAssoc);
            break;
            case "Inheritance":
            var newInh = {
                id: cid,
                type: name,
                superClasses: getIdFromClasses(element.attr('customAttr/superClasses')),
                subClasses: getIdFromClasses(element.attr('customAttr/subClasses')),
                position: element.position(),
                size: element.size()
            }
            inheritances.push(newInh);
            break;
            //default: console.log("def"); console.log(element)
        }
    }
    return [classes,inheritances,assocWithClass];
}

function getIdFromClasses(classes) {
    var ids = [];
    for (var i = classes.length - 1; i >= 0; i--) {
        ids.push(classes[i].cid);
    }
    return ids;
}

function getClassAssociation(assoc){
    var links = graphMain.getLinks();
    var classAssoc = null;
    for (var i = 0; i < links.length; i++) {
        if (links[i].attr('customAttr/type')=='classAssociation'){
            if (links[i].source().id==assoc.id) {
                classAssoc = graphMain.getCell(links[i].target().id);
                break;
            }else if(links[i].target().id==assoc.id){
                classAssoc = graphMain.getCell(links[i].source().id);
                break;
            }
        }
    }
    return classAssoc.cid;
}


function getInfoLink(link){
    var info;
    if (link.attr('customAttr/type')=='binaryAssociation'){
        info = {
            cardOrigin: link.attributes.labels[0].attrs.text.text,
            cardDestino: link.attributes.labels[1].attrs.text.text,
            roleOrigin: link.attributes.labels[2].attrs.text.text,
            roleDestiny: link.attributes.labels[3].attrs.text.text,
            nameAssociation: link.attributes.labels[4].attrs.text.text
        }
    }else if (link.attr('customAttr/type')=='class2rombo'){
        info = {
            cardOrigin: link.attributes.labels[0].attrs.text.text,
            roleOrigin: link.attributes.labels[1].attrs.text.text
        }
    }
    return info;
}


////////////////////////////
/////////////////IMPORT JSON
////////////////////////////

function importJSON() {
    $('#importJSON').css({
        display: "block",
    });
    $('#txtImportJSON').val(JSON.stringify(jsonTest));
}

/**
 Parse a diagram in JSON program. 

 @param str {string} The JSON in string.
*/
function load_json_str(str) {
    json = JSON.parse(str);
    if (json!=''){
        var classes = new Map();
        for (var i = json.classes.length - 1; i >= 0; i--) {
            classes.set(json.classes[i].id, createClass(json.classes[i]));
        }
        var inheritances = new Map();
        for (var i = json.inheritances.length - 1; i >= 0; i--) {
            inheritances.set(json.inheritances[i].id, createInheritance(json.inheritances[i], classes));
        }

        var rombos = new Map();
        for (var i = json.associationWithClass.length - 1; i >= 0; i--) {
            rombos.set(json.associationWithClass[i].id, createAssocWClass(json.associationWithClass[i], classes));
        }

        var links = new Map();
        for (var i = json.associations.length - 1; i >= 0; i--) {
            links.set(json.associations[i].id, createAssociation(json.associations[i], classes, rombos));
        }

        for (var i = json.associations.length - 1; i >= 0; i--) {
            if (json.associations[i].type == 'inheritanceRole'){
                createLinkRoles(json.associations[i], links);
            }
        }
    }
    
}

function loadJSON() {
    var json = $('#txtImportJSON').val();
    $('#importJSON').css({
        display: "none",
    });
    load_json_str(json);
}

/*Crea las clases*/
function createClass(cls) {
    var protclass = getElementFromPalette("Class");
    var newClass = protclass.clone();
    graphMain.addCell(newClass);
    newClass.attributes.name = cls.name;
    newClass.attributes.attributes = cls.attributes;
    newClass.attributes.methods = cls.methods;
    newClass.attr('data-class-url', cls.url);
    newClass.attr('data-attr-iris', cls.attribute_iris);
    newClass.position(cls.position.x, cls.position.y);
    newClass.size(cls.size.width, cls.size.height);
    //lanza un evento para que actualice los datos del objeto visual class.    
    newClass.trigger('change:name');
    return newClass;
}

/*Crea una herencia y la linkea a sus superclases y subclases*/
function createInheritance(inh, classes) {
    var newInh = getElementFromPalette("Inheritance").clone();
    newInh.attr('text/text', inh.type);
    newInh.attr('customAttr/type', inh.type);
    //setear las superclases
    var supcls = [];
    for (var i = inh.superClasses.length - 1; i >= 0; i--) {
        supcls.push(classes.get(inh.superClasses[i]));
    }
    newInh.attr('customAttr/superClasses', supcls);
    var subcls = [];
    for (var i = inh.subClasses.length - 1; i >= 0; i--) {
        subcls.push(classes.get(inh.subClasses[i]));
    }
    newInh.attr('customAttr/subClasses', subcls);
    newInh.position(inh.position.x, inh.position.y);
    graphMain.addCell(newInh);

    /*Crea links entre herencia y superclases*/
    var superC;
    var newLink;
    for (var i = inh.superClasses.length - 1; i >= 0; i--) {
        superC = classes.get(inh.superClasses[i])
        newLink = createLink();
        connectLink(newLink, superC, newInh);
        setInheritance(newLink,true);
    }
    /*Crea links entre herencia y subclases*/
    var subC;
    for (var i = inh.subClasses.length - 1; i >= 0; i--) {
        subC = classes.get(inh.subClasses[i])
        newLink = createLink();
        connectLink(newLink, subC, newInh);
        setInheritance(newLink,false);
    }

    return newInh;
}

/*Crea las asociaciones con clases*/
function createAssocWClass(assocWClass, classes) {
    var rombo = getElementFromPalette("Nary").clone();
    rombo.position(assocWClass.position.x, assocWClass.position.y);
    graphMain.addCell(rombo);
    var clsA = classes.get(assocWClass.classAssociation);
    var newLink = createLinkClassDashed();
    connectLink(newLink, clsA, rombo);
    return rombo;
}

/*crea los links entre clases-clases y entre clases-rombos*/
function createAssociation(assoc, classes, rombos) {
    var newLink;
    if (assoc.type =='binaryAssociation'){
        newLink = createLinkClass(assoc.type);
        connectLink(newLink, classes.get(assoc.source), classes.get(assoc.target));
        setAttrsLinkClass2Class(newLink, assoc.info.cardOrigin, assoc.info.cardDestino,
         assoc.info.roleOrigin, assoc.info.roleDestiny, assoc.info.nameAssociation);
        addPortToLink(newLink);
    }else if (assoc.type=='class2rombo'){
       newLink = createLinkClass(assoc.type);
       connectLink(newLink, classes.get(assoc.source), rombos.get(assoc.target));
       setAttrsLinkClass2Rombo(newLink, -1, assoc.info.cardOrigin, assoc.info.roleOrigin);
       addPortToLink(newLink);
   }
   return newLink;
}

/*Crea los links para las asociaciones entre roles*/
function createLinkRoles(assoc, links) {
    var newLink;
    var portS;
    var portT;
    portS = links.get(assoc.source).attr('customAttr/port');
    portT = links.get(assoc.target).attr('customAttr/port');
    newLink = createLinkProperty2Property();
    newLink.source({id: portS.id, port: portS.attributes.ports.items[0].id});
    newLink.target({id: portT.id, port: portT.attributes.ports.items[0].id});
    newLink.addTo(graphMain);
    var linkView = newLink.findView(paper);
    linkView.addTools(addToolsToDefaultLink());

}

jsonTest = {
    "classes": [{
        "name": "Class1",
        "id": "c31",
        "attributes": ["atr1"],
        "methods": ["mtd1"],
        "position": {
            "x": 430,
            "y": 110
        },
        "size": {
            "width": 105,
            "height": 96
        }
    }, {
        "name": "Class2",
        "id": "c47",
        "attributes": [],
        "methods": [],
        "position": {
            "x": 259,
            "y": 420
        },
        "size": {
            "width": 105,
            "height": 80
        }
    }, {
        "name": "Class3",
        "id": "c48",
        "attributes": [],
        "methods": [],
        "position": {
            "x": 680,
            "y": 410
        },
        "size": {
            "width": 105,
            "height": 80
        }
    }, {
        "name": "Class4",
        "id": "c49",
        "attributes": [],
        "methods": [],
        "position": {
            "x": 429,
            "y": 530
        },
        "size": {
            "width": 105,
            "height": 80
        }
    }, {
        "name": "Class5",
        "id": "c191",
        "attributes": [],
        "methods": [],
        "position": {
            "x": 790,
            "y": 120
        },
        "size": {
            "width": 105,
            "height": 80
        }
    }],
    "inheritances": [{
        "id": "c103",
        "type": "",
        "superClasses": ["c31"],
        "subClasses": ["c47", "c48"],
        "position": {
            "x": 460,
            "y": 280
        },
        "size": {
            "width": 40,
            "height": 40
        }
    }],
    "associationWithClass": [{
        "id": "c43",
        "classAssociation": "c49",
        "position": {
            "x": 464,
            "y": 440
        },
        "size": {
            "width": 30,
            "height": 30
        }
    }],
    "associations": [{
        "info": {
            "cardOrigin": "1..6",
            "roleOrigin": "roleA"
        },
        "source": "c47",
        "target": "c43",
        "id": "c59",
        "type": "class2rombo"
    }, {
        "info": {
            "cardOrigin": "1..6",
            "roleOrigin": "roleB"
        },
        "source": "c48",
        "target": "c43",
        "id": "c67",
        "type": "class2rombo"
    }, {
        "info": {
            "cardOrigin": "1..6",
            "cardDestino": "1..5",
            "roleOrigin": "role1",
            "roleDestiny": "role2",
            "nameAssociation": "superRole"
        },
        "source": "c31",
        "target": "c191",
        "id": "c195",
        "type": "binaryAssociation"
    }, {
        "source": "c67",
        "target": "c195",
        "type": "inheritanceRole"
    }]
};


////////////////////////////////
/////////////////CHECK SEMANTICO
////////////////////////////////
var errorList = [];


function check() {
    cleanInference();
    var req = checkSemantica();
    var rr;
    // Gestor del evento que indica el final de la petición (la respuesta se ha recibido)
    req.addEventListener("load", function() {
    // La petición ha tenido éxito
    if (req.status >= 200 && req.status < 400) {
        //console.log(req.responseText);
        rr = req.responseText.toString();
        if (rr.length == 4){
         alert('Modelo consistente');
     } else {
            //quitar corchetes
            var classes = JSON.parse(rr);
            var i;
            for (i = 0; i<classes.length;i++){
                //console.log(classes[i]);
                var elem = graphMain.getCells();
                var j;
                for (j = 0; j<elem.length;j++){
                    //console.log(elem[j].attr('text/text'));
                    //console.log(classes[i]);
                    //console.log(classes[i].toUpperCase());
                    //console.log(elem[j].attr('text/text').toUpperCase());
                    if (getType(elem[j])!='Error'){
                        if (getType(elem[j])=='Class'){
                            if (elem[j].attr('.uml-class-name-text/text').toUpperCase() == classes[i].toUpperCase()){
                                markElement(elem[j],'semantico');
                            }
                        }
                    }                }
                }
            }
        } else {
        // Se muestran informaciones sobre el problema ocasionado durante el tratamiento de la petición
        console.error(req.status + " " + req.statusText);
    }
});
}


//para consultar al razonador
function checkSemantica() {
    // Creación de la petición HTTP
    var req = new XMLHttpRequest();
    // Petición HTTP POST asíncrona si el tercer parámetro es "true" o no se especifica
    req.open("POST", "http://localhost:3000/", true);
    // Envío de la petición
    var q = exportJSON();
    console.log(q);
    var query = '{"type": "check","data": '+q+'}';
    req.send(query);
    // Gestor del evento que indica que la petición no ha podido llegar al servidor
    req.addEventListener("error", function(){
      console.error("Error de red"); // Error de conexión
  });
    return req;
}


function cleanInference() {
    var elements = graphMain.getCells();
    for (var i = 0; i < elements.length; i++) {
        unmarkElement(elements[i]);
    }
}


function markElement(element,tipo) {
 var color;
 switch(tipo) {
    case 'semantico':
    color = 'red';
    break;
    case 'sintactico':
    color = 'yellow';
    break;
    case 'selectGeneralizationType':
    color = 'blue';
    break;
}
element.attr('.uml-class-name-rect/stroke',color);
element.attr('.uml-class-name-rect/stroke-width',2);
    //element.attr('.outer/stroke',color);
}

function unmarkElement(element) {
    var defaultStroke;
    for (var i = 0; i < paletteElements.length; i++) {
        if (paletteElements[i].attributes.type.includes(element.attributes.type)) {
            //obtiene el stroke por defecto del elemento. Se basa en el valor que tiene en la paleta
            defaultStroke = paletteElements[i].attr('.uml-class-name-rect/stroke');
        }
    }
    element.attr('.uml-class-name-rect/stroke',defaultStroke);
    element.attr('.uml-class-name-rect/stroke-width',0.5);
}


/*para que los div se acomoden al tamaño de la ventana*/
$(window).resize(function () {
    //$("#palette").width($(window).width()*0.25);
    //$("#palette").height($(window).height());
    $("#paper").width($(window).width());
    $("#paper").height($(window).height());
});

//ejemplo para testear
/*var ent1 = paletteElements[0].clone();
var ent2 = paletteElements[0].clone();
var ent3 = paletteElements[0].clone();
var ent4 = paletteElements[0].clone();
var attk1 = paletteElements[3].clone();
var attk2 = paletteElements[3].clone();
var attk3 = paletteElements[3].clone();
var attk4 = paletteElements[3].clone();
graphMain.addCells([ent1,ent2,ent3,ent4,attk1,attk2,attk3,attk4]);
var link1 = createLink();
var link2 = createLink();
var link3 = createLink();
var link4 = createLink();
connectLink(link3, ent1,attk3);
connectLink(link4, ent2,attk4);
connectLink(link1, ent3,attk1);
connectLink(link2, ent4,attk2);
*/
