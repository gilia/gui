# import.coffee --
# Copyright (C) 2019 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Create a class from its JSON representation
#
# @param cls {object} The class as an imported JSON.
# @param classmap {Map} A Map between imported cid and Cell objects.
create_class = (cls, classmap) ->
    # newClass = getElementFromPalette("Class").clone()

    # newClass.attributes.name = cls.name
    # newClass.attributes.attributes = cls.attributes
    # newClass.attributes.methods = cls.methods

    # newClass.position cls.position.x, cls.position.y
    # newClass.size cls.size.width, cls.size.height

    # graphMain.addCell newClass
    
    # # lanza un evento para que actualice los datos del objeto visual class.
    # newClass.trigger 'change:name'

    newClass = createClass cls
    classmap.set cls.id, newClass


# Create the links between the circle Is-A to the superclasses.
#
# @param inh {Cell} The circle cell representing the inheritance.
# @param lst_classes {array[Cell]} An array of cells.
make_superclass_links = (inh, lst_classes) ->
    for cls in lst_classes
        do (cls) ->
            if cls?
                new_link = createLink()
                connectLink new_link, cls, inh
                setInheritance new_link, true

# Create the links between the circle Is-A to the subclasses.
#
# @param inh {Cell} The circle cell representing the inheritance.
# @param lst_classes {array[Cell]} An array of cells.
make_subclass_links = (inh, lst_classes) ->
    for cls in lst_classes
        do (cls) ->
            if cls?
                new_link = createLink()
                connectLink new_link, cls, inh
                setInheritance new_link, false

# Create the Is-A relationships from a JSON representation.
#
# @param inh {object} A JSON representation of an inheritance.
create_inheritance = (inh, classmap) ->
    newInh = getElementFromPalette("Inheritance").clone()

    newInh.attr 'text/text', inh.type
    newInh.attr 'customAttr/type', inh.type

    # Search cells from superclasses id
    supcls = inh.superClasses.map (parent_id) ->
        classmap.get parent_id

    # set them at the custom attribute
    newInh.attr 'customAttr/superClasses', supcls

    # Search cells from subclasses id
    subcls = inh.subClasses.map (child_id) ->
        classmap.get child_id
        
    newInh.attr 'customAttr/subClasses', subcls
    
    newInh.position inh.position.x, inh.position.y

    graphMain.addCell newInh

    make_superclass_links newInh, supcls
    make_subclass_links newInh, subcls

    newInh

# Create a class from its JSON representation
#
# @param assoc {object} A JSON representation of an association.
create_association = (assoc, classmap) ->
    new_link = createLinkClass assoc.type, assoc.id
    # new_link.id = assoc.id # already setted by createLinkClass
    
    source_cell = classmap.get assoc.source
    target_cell = classmap.get assoc.target

    connectLink new_link, source_cell, target_cell

    setAttrsLinkClass2Class new_link, \
        assoc.info.cardOrigin, assoc.info.cardDestino, \
        assoc.info.roleOrigin, assoc.info.roleDestiny, \
        assoc.info.nameAssociation

    new_link.attr 'data-role-origin-iri', assoc.info.roleOriginIRI
    new_link.attr 'data-role-target-iri', assoc.info.roleTargetIRI
    new_link.attr 'data-assoc-iri', assoc.info.assocIRI

    addPortToLink new_link

    new_link


export import_json = (json) ->
    classmap = new Map()
    
    create_class a_class, classmap for a_class in json.classes
    create_inheritance inheritance, classmap for inheritance in json.inheritances
    create_association assoc, classmap for assoc in json.associations


