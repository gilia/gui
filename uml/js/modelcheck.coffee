# modelcheck.coffee --
# Copyright (C) 2019 GILIA

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.



exports = exports ? this
exports.modelcheck = exports.modelcheck ? {}

# This modules present basic modifications to the diagram.
#
# When the reasoner returns its answer, the diagram must be modified according
# to it.

# @mixin
exports.modelcheck =

    # Mark all the classes as satisfiable or insatisfiable
    #
    # @param satisf {array[string]} The class names for satisfiable
    #   classes (not their ids).
    # @param insatisf {array[string]} The class names for insatisfiable
    #   classes (not their ids).
    mark_all: (satisf, insatisf) ->
        cells = graphMain.getCells()
        for c in cells
            do (c) ->
                if c.attributes.name in satisf
                    modelcheck.satisf_class c.cid
                if c.attributes.name in insatisf
                    modelcheck.insatisf_class c.cid

    # Mark a class as satisfiable.
    #
    # @param cid {string} The class id. See `graphMain.getCells()`.
    satisf_class: (cid) ->
        model = graphMain.getCell cid
        view = paper.findViewByModel model

        model.attr '.uml-class-name-rect',
            fill: '#88ff88'
        model.attr '.uml-class-methods-rect',
            fill: '#88ff88'
        model.attr '.uml-class-attrs-rect',
            fill: '#88ff88'

        view.update()

    # Mark a class as insatisfiable.
    #
    # @params cid {string} The class id. See `graphMain.getCells()`.
    insatisf_class:  (cid) ->
        model = graphMain.getCell cid
        view = paper.findViewByModel model

        model.attr '.uml-class-name-rect',
            fill: '#ff8888'
        model.attr '.uml-class-methods-rect',
            fill: '#ff8888'
        model.attr '.uml-class-attrs-rect',
            fill: '#ff8888'

        view.update()

    # Erase any insatisfiable or satisfiable mark.
    #
    # @params cid {string} The class id.
    clear_class: (cid) ->
        model = graphMain.getCell cid
        view = paper.findViewByModel model

        model.attr '.uml-class-name-rect',
            fill: '#ff8450'
        model.attr '.uml-class-methods-rect',
            fill: '#fe976a'
        model.attr '.uml-class-attrs-rect',
            fill: '#fe976a'

        view.update()

    # Clear all classes of satisfiable/insatisfiable mark.
    clear_all: () ->
        models = graphMain.getCells()
        for model in models
            do (model) ->
                modelcheck.clear_class model.cid
