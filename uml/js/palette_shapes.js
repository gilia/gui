/*
Este archivo define los elementos de la paleta.
*/

/////////////////////////////////////////////
/////DEFINICION DE LOS ELEMENTOS DE LA PALETA
/////////////////////////////////////////////

function paletteElements() {
    return [umlclass, nary, inheritance];
}

function paletteElementsReduced() {
    return [umlclass, nary, inheritance];
}

var gridSize = $('#palette').width();

function sortPalette(horizontal, size, elements) {
    var col = 0;
    var row = 0;
    elements.forEach(function (element) {
        element.attributes.position.x = gridSize * col + gridSize / 2 - element.attributes.size.width / 2;
        element.attributes.position.y = gridSize * row + gridSize / 2 - element.attributes.size.height / 2;
        if (horizontal) {
            if (col < size - 1) {
                col++
            } else {
                col = 0;
                row++;
            }
        } else {
            if (row < size - 1) {
                row++
            } else {
                row = 0;
                col++;
            }
        }
    });
}



var umlclass = new joint.shapes.uml.Class({
    position: {
        x: gridSize * 0 + gridSize / 2 - 40,
        y: gridSize * 0 + gridSize / 2 - 20
    },
    size: {
        width: 100,
        height: 70
    },
    name: 'Class',
    /*attributes: ['houseNumber: Integer','streetName: String','town: String','postcode: String'],*/
    attributes: [],
    attrs: {
        '.uml-class-name-rect': {
            fill: '#ff8450',
            stroke: '#fff',
            'stroke-width': 0.5
        },
        '.uml-class-attrs-rect': {
            fill: '#fe976a',
            stroke: '#fff',
            'stroke-width': 0.5
        },
        '.uml-class-methods-rect': {
            fill: '#fe976a',
            stroke: '#fff',
            'stroke-width': 0.5
        },
        '.uml-class-attrs-text': {
            ref: '.uml-class-attrs-rect',
            'ref-y': 0.5,
            'y-alignment': 'middle'
        },
        'data-class-url': 'http://crowd.fi.uncoma.edu.ar/crowd2/ontology/',
        customAttr:{
            isCompact: false
        }
    }
});

var nary = new joint.shapes.erd.Relationship({
    position: {
        x: gridSize * 1 + gridSize / 2 - 30,
        y: gridSize * 0 + gridSize / 2 - 30
    },
    attrs: {
        text: {
            fill: '#ffffff',
            text: '',
            'letter-spacing': 0,
            style: {
                'font-size': '10px',
                'text-shadow': '1px 0 1px #333333'
            }
        },
        '.outer': {
            fill: '#648040',
            stroke: 'none',
            filter: {
                name: 'dropShadow',
                args: {
                    dx: 0,
                    dy: 2,
                    blur: 1,
                    color: '#333333'
                }
            }
        }
    },
    size: {
        width: 30,
        height: 30
    },
});

joint.shapes.erd.Attribute.define('erd.Inheritance', {
    position: {
            x: gridSize * 4 + gridSize / 2 - 30,
            y: gridSize * 0 + gridSize / 2 - 20
    },
    attrs: {
        text: {
            fill: '#ffffff',
            text: '',
            fontWeight: 'bold',
            'letter-spacing': 0,
            style: {
                    'text-shadow': '1px 0 1px #333333'
                }
        },
        '.outer': {
            fill: '#51c0ca',
            stroke: 'none',
            filter: {
                name: 'dropShadow',
                args: {
                    dx: 0,
                    dy: 2,
                    blur: 1,
                    color: '#333333'
                }
            }
        },
        '.inner': {
        },
        customAttr: {
            type: 0,
            superClasses: [],
            subClasses: []
        }
    },
    size: {
        width: 40,
        height: 40
    },
});

var inheritance = new joint.shapes.erd.Inheritance();
