/*
Esta archivo contiene herramientas para los componentes del diagrama uml. 
No implementa ninguna funcionalidad relacionada al paper principal donde
se crea el diagrama.
- Se definen aspectos visuales de la paleta.
    *posicion, tamanio, opacidad, etc...
    *La funcionalidad drag and drop de la paleta esta en el archivo uml.js.
- Se definen menus de opciones para editar los atributos de los elementos graficos.
    *Atributos, metodos, roles, cardinalidades, tipos de herencia, etc..
- Se define toolbar para editar visualmente los elementos graficos. 
    *Boton para borrar un elemento, boton para ocultar/mostrar atributos y metodos de una clase,
    boton para seleccionar super entities. 

Hay variables globales compartidas entre este archivo y uml.js:
- actualElement
- paletteElements
*/

////////////////////////////////////
///////////////////PALETA
///////////////////////////////////
//graph para la paleta
var paletteGraph = new joint.dia.Graph();
//graph para manejar la paleta
var palette = new joint.dia.Paper({
  el: document.getElementById('palette'),
  width: $('#palette').width(),
  height: $('#palette').height(),
  model: paletteGraph,
  interactive: false,
        //gridSize: 100,
        /*drawGrid:
    {
        name: 'mesh',
        args: [
    {
        color: 'white',
        thickness: 1
        }
        ]
    }*/
});

//para mover la paleta
var dragStartPositionPalette;
palette.on('blank:pointerdown', function (event, x, y) {
    if (!fixedPalette) {
        dragStartPositionPalette = {
            x: x,
            y: y
        };

        $('body').on('mousemove.fly', function (event) {
            if (dragStartPositionPalette != null) {
                $("#palette").offset({
                    left: event.pageX - dragStartPositionPalette.x, //$("#palette").width() / 2,
                    top: event.pageY - dragStartPositionPalette.y //$("#palette").height() / 2
                });
            }
        });

        $('body').on('mouseup.fly', function (e) {
            dragStartPositionPalette = null;
            $('body').off('mousemove.fly').off('mouseup.fly');
        });
    }
});

// herramientas para la paleta
var fixedPalette = true;
var opacityPalette = false;
var extendedPalette = false;
var horizontalPalette = false;

var paletteTools = $('<div id="paletteTools" class="toolbar-palette">');
paletteTools.append('<div id="paletteFixButton" class="tools tools-palette-fix" onclick="paletteFix()"><a class="tooltips" href="#"><i class="material-icons">lock</i><span>Unlock palette translate</span></a></div>');
paletteTools.append('<div id="paletteResetButton" class="tools tools-palette-reset" onclick="paletteReset()"><a class="tooltips" href="#"><i class="material-icons">picture_in_picture_alt</i><span>Reset default position</span></a></div>');
paletteTools.append('<div id="paletteOpacityButton" class="tools tools-palette-opacity" onclick="paletteOpacity()"><a class="tooltips" href="#"><i class="material-icons">visibility</i><span>Change palette opacity</span></a></div>');
paletteTools.append('<div id="paletteExtendButton" class="tools tools-palette-extend" onclick="paletteExtend()"><a class="tooltips" href="#"><i class="material-icons">add_circle</i><span>Extend palette elements</span></a></div>');
//paletteTools.append('<div id="paletteRotateButton" class="tools tools-palette-rotate" onclick="paletteRotate()"><i class="material-icons">rotate_90_degrees_ccw</i></div>');
paletteTools.css('display', 'none');

$('#palette').append(paletteTools);

$("#palette").mouseover(function () {
    paletteTools.css('display', 'block');
});

$("#palette").mouseleave(function () {
    paletteTools.css('display', 'none');
});

function paletteFix() {
    fixedPalette = !fixedPalette;
    if (fixedPalette) {
        document.getElementById("paletteFixButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">lock</i><span>Unlock palette translate</span></a>';
    } else {
        document.getElementById("paletteFixButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">lock</i><span>Lock palette translate</span></a>';
    }
}

function paletteReset() {
    var lastOpacity = document.getElementById("palette").style.opacity;
    var lastHeight = document.getElementById("palette").style.height;
    var lastWidth = document.getElementById("palette").style.width;
    document.getElementById("palette").style = null;
    document.getElementById("palette").style.opacity = lastOpacity;
    document.getElementById("palette").style.height = lastHeight;
    document.getElementById("palette").style.width = lastWidth;
}

function paletteOpacity() {
    opacityPalette = !opacityPalette;
    if (opacityPalette) {
        document.getElementById("paletteOpacityButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">visibility_off</i><span>Change palette opacity</span></a>';
        document.getElementById("palette").style.opacity = ".7";
    } else {
        document.getElementById("paletteOpacityButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">visibility</i><span>Change palette opacity</span></a>';
        document.getElementById("palette").style.opacity = "1";
    }
}

var paletteElements = paletteElements();
var paletteElementsReduced = paletteElementsReduced();
var actualElements = paletteElementsReduced;

sortPalette(false, 3, paletteElements);

paletteGraph.addCells(actualElements);

function paletteExtend() {
    extendedPalette = !extendedPalette;
    if (extendedPalette) {
        document.getElementById("paletteExtendButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">remove_circle</i><span>Reduce palette elements</span></a>';
        actualElements = paletteElements;
        if (horizontalPalette) {
            document.getElementById("palette").style.height = "201px";
        } else {
            document.getElementById("palette").style.width = "201px";
        }
    } else {
        document.getElementById("paletteExtendButton").innerHTML = '<a class="tooltips" href="#"><i class="material-icons">add_circle</i><span>Extend palette elements</span></a>';
        actualElements = paletteElementsReduced;
        if (horizontalPalette) {
            document.getElementById("palette").style.height = "101px";
        } else {
            document.getElementById("palette").style.width = "101px";
        }
    }
    paletteGraph.clear();
    paletteGraph.addCells(actualElements);
}

function paletteRotate() {
    horizontalPalette = !horizontalPalette;
    sortPalette(horizontalPalette, 3, paletteElements);
    sortPalette(horizontalPalette, 3, paletteElementsReduced);
    paletteGraph.clear();
    paletteGraph.addCells(actualElements);
    /*if (horizontalPalette) {
    document.getElementById("palette").className = "palette-horizontal";
    } else {
    document.getElementById("palette").className = "palette-vertical";
    }
    document.getElementById("palette").style = null;
    document.getElementById("paletteTools").style = null;*/
    var actualWidth = document.getElementById("palette").style.width;
    var actualHeight = document.getElementById("palette").style.height;
    document.getElementById("palette").style.width = actualHeight;
    document.getElementById("palette").style.height = actualWidth;
}
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////TOOLBAR PARA LAS HERRAMIENTAS VISUALES (alrededor de los cell)
/////////////////////////////////////////////////////////////////////////////////////////
var actualElement = null;

var tools = $('<div class="toolbarElement"></div>');
tools.append('<div id="elementDeleteButton" class="tools tools-delete" onclick="elementDelete()"><a class="tooltips" href="#"><i class="material-icons">delete_forever</i><span>Remove the element</span></a></div>');
//tools.append('<div class="tools tools-clearlink">C</div>');
//tools.append('<div class="tools tools-newnext">N</div>');
tools.append('<div id="selectSuperEntity" class="tools tools-select-super-entity" onclick="selectSuperEntity(event)"><a class="tooltips" href="#"><i class="material-icons">expand_less</i><span>Define super entity</span></a></div>');
tools.append('<div id="compactClass" class="tools tools-compactClass" onmousedown="compactClass()"><a class="tooltips" href="#"><i class="material-icons">calendar_view_day</i><span>show/hide attributes and methods</span></a></div>');
tools.append('<div id="elementLinkButton" class="tools tools-link" onmousedown="elementLink(event)"><a class="tooltips" href="#"><i class="material-icons">trending_up</i><span>Connect to other element</span></a></div>');
tools.append('<div id="elementDuplicateButton" class="tools tools-duplicate" onclick="elementDuplicate()""><a class="tooltips" href="#"><i class="material-icons">file_copy</i><span>Duplicate the element</span></a></div>');
tools.css({
    display: 'none'
});

$('body').append(tools);
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////
///////////////MENU OPCIONES PARA EDITAR LOS ATRIBUTOS DE CELL
//////////////////////////////////////////////////////////////

$('body').append('<div id="menues"><div id=encabezadoMenu></div><div id=menuOpciones></div></div>');

$('#menues').focusin(function(){
    hideElementTools();
});

$('#encabezadoMenu').append('<h4>Opciones</h4><button id="btnMinimizeMenues" name="minimizeMenues" onclick=minimizeMenues() type="button" value="-" class="btn btn-sm btn-secondary mx-2"><i class="fas fa-window-minimize"></i></button>');
function minimizeMenues(){
    if ($('#btnMinimizeMenues').val() == '-'){
        $('#menuOpciones').css({
            display: 'none'
        });
        $('#menues').css({
            height: 35
        });
        $('#btnMinimizeMenues').val('+');
    }else{
        $('#menuOpciones').css({
            display: 'block'
        });
        $('#menues').css({
            height: 500
        });
        $('#btnMinimizeMenues').val('-');
    }
}

//para evitar que el puntero quede focuseando en un input cuando se selecciona un elemento del grafo. 
$('#paper').mouseup(function(evt){
    if (evt.target.tagName=='tspan'){
        $(document.activeElement).blur();
    }
});

//menu con opciones de clases
/*
divmenu = $("<div>").attr('id', 'menuClass').attr('class', 'm-2');
$('#menuOpciones').append(divmenu);

diviri = $('<div>').attr({
    'class': 'input-group'
});
divmenu.append(diviri);

div = $('<div>').attr({
    'class': 'input-group-prepend'
}).append(
    $('<span>').attr('class', 'input-group-text').append(
        $('<i>').attr('class', 'fas fa-link')));
diviri.append(div);

urlinput = $("<input>").attr({
    'class': 'form-control form-control-sm',
    'id': 'class-url'});
diviri.append(urlinput);

divmenu.append('<div id="changeNameClass"><input id="classRenameInput" type="text" class="form-control form-control-sm"></div>');

divattrs = $('<div>').attr('class', 'my-1');
divattrs.append('<div id="listAttributes" class="mx-2"></div>');
divattrs.append('<button id="btnAddAttributeClass" name="addAttribute" onclick=addAttribute() type="button" class="btn btn-sm btn-block btn-secondary" value="add attr"><i class="fas fa-plus"></i> Attr.</button>');

divmenu.append(divattrs);


divmethods = $('<div>').attr('class', 'my-1');
divmethods.append('<div id="listMethods" class="mx-2"></div>');
divmethods.append('<button id="btnAddMethodClass" name="addMethod" onclick=addMethod() type="button" class="btn btn-sm btn-secondary btn-block" value="add method"><i class="fas fa-plus"></i> Method</button>');

divmenu.append(divmethods);

donecancel = $('<div>').attr('class', 'btn-group my-2');
donecancel.append('<button id="btnConfirmMenuClass" name="done" onclick=confirmMenuClass() type="button" class="btn btn-sm btn-success"  value="done"><i class="fas fa-check"></i> Done</button>');
donecancel.append('<button id="btnCancelMenuClass" name="done" onclick=cancelMenuClass() type="button" class="btn btn-sm btn-danger"  value="cancel"><i class="fas fa-times"></i> Cancel</button>');

$("#menuClass").append(donecancel);

$('#menuClass').css({
    display: 'none'
});
*/

//menu con opciones de links
$('#menuOpciones').append('<div id="menuLink"></div>');
divmenu = $("#menuLink")

divlink = $('<div>').attr({
    'id' : 'changeLabelLink',
    'class' : 'mx-2'
});
divmenu.append(divlink);

label = $('<label>').attr(
    {
        'class' : 'labelNameAssociation',
        'for' : 'labelNameAssociation',
    })
    .html('Nombre asociación');

input_iri = $('<input>').attr({
    'class' : 'form-control form-control-sm',
    'id' : 'assoc-iri',
    'type' : 'text'
});
input = $('<input>').attr(
    {
        'class' : 'labelNameAssociation form-control form-control-sm',
        'id' : 'labelNameAssociation',
        'name' : 'labelNameAssociation',
        'type' : 'text'
    });


divlink.append(label);
divlink.append(input);
divlink.append(input_iri);


label = $('<label>')
    .attr({
        'class' : 'labelCardOrigin',
        'for' : 'labelCardOrigin'
    })
    .html('Cardinalidad origen');
input = $('<input>').attr(
    {
        'class' : 'labelCardOrigin form-control form-control-sm',
        'id' : 'labelCardOrigin',
        'name' : 'labelCardOrigin',
        'type' : 'text'
    });

divlink.append(label);
divlink.append(input);

label = $('<label>').attr(
    {
        'class' : 'labelCardDestino',
        'for' : 'labelCardDestino',
    })
    .html('Cardinalidad destino');
input = $('<input>').attr(
    {
        'class' : 'labelCardDestino form-control form-control-sm',
        'id' : 'labelCardDestino',
        'name' : 'labelCardDestino',
        'type' : 'text'
    });

divlink.append(label);
divlink.append(input);

label = $('<label>').attr(
    {
        'class' : 'labelRoleOrigin',
        'for' : 'labelRoleOrigin',
    })
    .html('Role origen');
input = $('<input>').attr(
    {
        'class' : 'labelRoleOrigin form-control form-control-sm',
        'id' : 'labelRoleOrigin',
        'name' : 'labelRoleOrigin',
        'type' : 'text'
    });
input_iri = $('<input>').attr(
    {
        'class' : 'form-control form-control-sm',
        'id' : 'assoc-role-origin-iri',
        'type' : 'text'
    });


divlink.append(label);
divlink.append(input);
divlink.append(input_iri);

label = $('<label>').attr(
    {
        'class' : 'labelRoleDestino',
        'for' : 'labelRoleDestino',
    })
    .html('Role destino');
input = $('<input>').attr(
    {
        'class' : 'labelRoleDestino form-control form-control-sm',
        'id' : 'labelRoleDestino',
        'name' : 'labelRoleDestino',
        'type' : 'text'
    });
input_iri = $('<input>').attr(
    {
        'class' : 'form-control form-control-sm',
        'id' : 'assoc-role-target-iri',
        'type' : 'text'
    });

divlink.append(label);
divlink.append(input);
divlink.append(input_iri);

divbtn = $('<div>').attr({
    'class': 'btn-group btn-group-sm my-2',
    'role' : 'group'
});
btn = $('<button>').attr({
    'class' : 'btn btn-success',
    'id' : 'btnConfirmMenuLink',
    'name' : 'done',
    'onclick' : 'confirmMenuLink()',
    'type' : 'button',
    'value' : 'done'
}).html('Done');

divbtn.append(btn);

btn = $('<button>').attr({
    'class' : 'btn btn-danger',
    'id' : 'btnCancelMenuLink',
    'name' : 'cancel',
    'onclick' : 'cancelMenuLink()',
    'type' : 'button',
    'value' : 'cancel'
}).html('Cancel');

divbtn.append(btn);

divlink.append(divbtn);

// $('#menuLink').append('<input id="btnConfirmMenuLink" name="done" onclick=confirmMenuLink() type="button" value="done"><input id="btnCancelMenuLink" name="cancel" onclick=cancelMenuLink() type="button" value="cancel">');
// divlink.hide();
$('#menuLink').css({
    display: 'none'
});

//menu con opciones de generalizacion
$('#menuOpciones').append('<div id="menuGeneralization"></div>');
$('#menuGeneralization').append('<label for="labelGeneralizationType">Tipo generalizacion</label><select id="selectGeneralizationType" name="selectGeneralizationType"><option value="">Default</option><option value="c">Covering</option><option value="d">disjoint</option><option value="c/d">covering/disjoint</option>');
$('#menuGeneralization').append('<input id="btnConfirmMenuGeneralization" name="done" onclick=confirmMenuGeneralization() type="button" value="done"><input id="btnCancelMenuGeneralization" name="cancel" onclick=cancelMenuGeneralization() type="button" value="cancel">');
$('#menuGeneralization').css({
    display: 'none'
});


disableTool('#elementLinkButton');
disableTool('#elementDuplicateButton');
$('#elementLinkButton').css({
    display: "none"
});

$('#elementDuplicateButton').css({
    display: "none"
});

function disableTool(tool) {
    $(tool).css({
        'background': '#787575',
        'pointer-events': 'none',
    });
}

$('body').on('keydown', function (e) {
    if (e.key === "Delete") {
        if (actualElement != null) {
            elementDelete();
        }
    }
});


$('#paper').on('mousewheel DOMMouseScroll', function (evt) {
    hideElementTools();
});

//oculta las herramientas de los elementos
function hideElementTools() {
    tools.css('display', 'none');
    actualElement = null;
}

function elementDelete() {
    var cell = graphMain.getCell(tools.attr('elementid'));
    cell.remove();
    //actualElement.remove();
    hideElementTools();
}

//oculta cualquier div dentro del div menuOpciones. Usado para ocultar los menus link, class, etc.
function ocultarMenues(){
    var mns = $('#menuOpciones').children();
    for (var i = mns.length - 1; i >= 0; i--) {
        if (mns[i].style.display=='block'){
            mns[i].style.display = 'none';
        }
    }
}

//////////////////////////////////////////////////////
//////////////////////////////////////////////////////
//////////////////////////////////////////////////////


//////////////////////////////////////////
/////////////////// MENU PARA LOS LINKS
//////////////////////////////////////////
//menuLink gestiona las funcionalidades de un link
var actualLink = null;
function menuLink(link) {
    hideElementTools();
    actualLink = link;
    default_iri = "http://crowd.fi.uncoma.edu.ar/crowd2/ontology/";
    if (link.model.attr('customAttr/type')=='binaryAssociation'){
        $('.labelCardOrigin').val(
            link.model.attributes.labels[0].attrs.text.text);
        $('.labelCardDestino').val(
            link.model.attributes.labels[1].attrs.text.text);
        $('.labelRoleOrigin').val(
            link.model.attributes.labels[2].attrs.text.text);
        $('.labelRoleDestino').val(
            link.model.attributes.labels[3].attrs.text.text);
        $('.labelNameAssociation').val(
            link.model.attributes.labels[4].attrs.text.text);
        $('.labelCardDestino').css({display:"block"});
        $('.labelRoleDestino').css({display:"block"});
        $('.labelNameAssociation').css({display:"block"});
        $("#menuLink").css({display:"block"});

        // Set IRI information
        iri = link.model.attr('data-assoc-iri');
        if (iri == null || iri == undefined || iri == ""){
            iri = default_iri;
        }
        $("#assoc-iri").val(iri);
        
        iri = link.model.attr('data-role-origin-iri');
        if (iri == null || iri == undefined || iri == ""){
            iri = default_iri;
        }
        $("#assoc-role-origin-iri").val(iri);
        
        iri = link.model.attr('data-role-target-iri');
        if (iri == null || iri == undefined || iri == ""){
            iri = default_iri;
        }
        $("#assoc-role-target-iri").val(iri);
        
    }else if (link.model.attr('customAttr/type')=='class2rombo'){
        $('.labelCardOrigin').val(link.model.attributes.labels[0].attrs.text.text);
        $('.labelRoleOrigin').val(link.model.attributes.labels[1].attrs.text.text);
        $('.labelCardDestino').css({display:"none"});
        $('.labelRoleDestino').css({display:"none"});
        $('.labelNameAssociation').css({display:"none"});
        $("#menuLink").css({display:"block"});
    }
}

//para setear los labels de un link una vez presionado el botton en #changeLabel
function confirmMenuLink(){
    if (actualLink.model.attr('customAttr/type')=='binaryAssociation'){
        changeLabelCardinalityBinaryAssociation();
        changeLabelRoleBinaryAssociation();
        changeNameAssociation();
    }else{
        changeLabelCardinalityClassAssociation();
        changeLabelRoleClassAssociation();
    }

    actualLink = null;
    $("#menuLink").css({display:"none"});
}

function changeLabelCardinalityBinaryAssociation(){
    if (actualLink!=null){
        if ($('#labelCardOrigin').val()!='undefined'){
            actualLink.model.label(0, {
                attrs: {
                    text: {
                        text: $('#labelCardOrigin').val()
                    }
                }
            });
        }
        if ($('#labelCardDestino').val()!='undefined'){
            actualLink.model.label(1, {
                attrs: {
                    text: {
                        text: $('#labelCardDestino').val()
                    }
                }
            });
        }
    }
}

function changeLabelRoleBinaryAssociation(){
    if (actualLink!=null){
        if ($('#labelRoleOrigin').val()!='undefined'){
            actualLink.model.label(2, {
                attrs: {
                    text: {
                        text: $('#labelRoleOrigin').val()
                    }
                }
            });
        }
        if ($('#labelRoleDestino').val()!='undefined'){
            actualLink.model.label(3, {
                attrs: {
                    text: {
                        text: $('#labelRoleDestino').val()
                    }
                }
            });
        }

        actualLink.model.attr('data-role-target-iri',
                              $('#assoc-role-target-iri').val());
        actualLink.model.attr('data-role-origin-iri',
                              $('#assoc-role-origin-iri').val());
      
    }
}

function changeLabelCardinalityClassAssociation(){
    if (actualLink!=null){
        if ($('#labelCardOrigin').val()!='undefined'){
            actualLink.model.label(0, {
                attrs: {
                    text: {
                        text: $('#labelCardOrigin').val()
                    }
                }
            });
        }
    }
}

function changeLabelRoleClassAssociation(){
    if (actualLink!=null){
        if ($('#labelRoleOrigin').val()!='undefined'){
            actualLink.model.label(1, {
                attrs: {
                    text: {
                        text: $('#labelRoleOrigin').val()
                    }
                }
            });
        }
    }
}

function changeNameAssociation(){
    if (actualLink!=null){
        if ($('#labelNameAssociation').val()!='undefined'){
            actualLink.model.label(4, {
                attrs: {
                    text: {
                        text: $('#labelNameAssociation').val()
                    }
                }
            });
        }

        actualLink.model.attr('data-assoc-iri', $("#assoc-iri").val());
    }
}

function cancelMenuLink(){
    actualLink = null;
    $("#menuLink").css({display:"none"});
}

//////////////////////////////////////////
//////////////////////////////////////////
//////////////////////////////////////////

/////////////////////////////////////////
/////////////////// MENU PARA LAS CLASES
/////////////////////////////////////////
//menuClass gestiona las funcionalidades de una class
var actualClass = null;
function menuClass(cls) {
    hideElementTools();
    actualClass = cls;
    //setear la URL de la clase
    $("#class-url").val(actualClass.model.attr('data-class-url'));
    //setear el nombre de la clase en el textbox
    $('#classRenameInput').val(actualClass.model.attr('.uml-class-name-text/text'));
    //limpiar los atributos y crear los pertenecientes a esta clase
    ($("#listAttributes")).empty();
    if (actualClass.model.attributes.attributes.length!=0){
        //muestra todos los atributos
        var ats = actualClass.model.attributes.attributes;
        var attr_iri = actualClass.model.attr('data-attr-iris');
        if (attr_iri == undefined || attr_iri == null){
            attr_iri = [];
        }
        
        for (var i = 0; i <= ats.length - 1; i++) {
            addAttribute(cls, ats[i], attr_iri[i]);
        }
    }
    //limpiar los metodos y crear los pertenecientes a esta clase
    ($("#listMethods")).empty();
    if (actualClass.model.attributes.methods.length!=0){
        //muestra todos los atributos
        var ats = actualClass.model.attributes.methods;
        for (var i = 0; i <= ats.length - 1; i++) {
            addMethod(cls, ats[i]);
        }
    }
    //mostrar el menu de clases
    $("#menuClass").css({display:"block"});
    $('#classRenameInput').focus();
}

//btn agregar atributo
function addAttribute(cls,
                      value="",
                      iri=null){

    if (iri == undefined || iri == null){
        iri = "http://crowd.fi.uncoma.edu.ar/crowd2/ontology/";
    }
    var childs = ($("#listAttributes")).children().length+1;
    div_holder = $("#listAttributes");

    divattr = $('<div>').attr({
        'id': 'atts_' + actualClass.id + '_' + childs,
        'class': 'mb-1'
    });
    div_holder.append(divattr);

    irigrp = $('<div>').attr({
        'class': 'input-group input-group-sm'
    });
    divattr.append(irigrp);
    
    div = $('<div>').attr({
        'class': 'input-group-prepend'
    }).append(
        $('<span>').attr('class', 'input-group-text').append(
            $('<i>').attr('class', 'fas fa-link')));
    irigrp.append(div);
   
    input_iri = $('<input>').attr({
        'class' : 'attr-iri form-control form-control-sm',
        'id' : 'attr-iri' + actualClass.id + '-' + childs,
        'type' : 'text'
    }).val(iri);
    irigrp.append(input_iri);
    

    inputgrp = $('<div>').attr({
        'class': 'input-group input-group-sm'
    });
    divattr.append(inputgrp);
    
    attrinput = $('<input>').attr({
        'class': 'attributeClass form-control',
        'type': 'text'
    }).val(value);
    inputgrp.append(attrinput);

    divgrp = $('<div>').attr({
        'class': 'input-group-append'
    });
    inputgrp.append(divgrp);
    
    delbtn = $('<button>')
        .attr({
            'class': 'btnDeleteInputClass btn btn-sm btn-danger',
            'type': 'button',
            'value': '-',
            'onclick': 'deleteInput(atts_' + actualClass.id + '_' + childs + ')'
        })
        .append( $('<i>').attr('class', 'fas fa-trash'));
    divgrp.append(delbtn);

}

//btn agregar method
function addMethod(cls, value=""){
    var childs = ($("#listMethods")).children().length+1;

    divmethod = $('<div>').attr('id', 'mts_' + actualClass.id + '_' + childs)
        .attr('class', 'input-group input-group-sm mb-1');
    
    divgrp = $('<div>').attr('class', 'input-group-append');
    
    methodinput=$('<input>').attr('class', 'methodClass form-control')
        .attr('type', 'text')
        .val(value);
    delbtn = $('<button>').attr({
        'class': 'btnDeleteInputClass btn btn-sm btn-danger',
        'type': 'button',
        'value': '-',
        'onclick': 'deleteInput(mts_' + actualClass.id + '_' + childs +')'
    }).append( $('<i>').attr('class', 'fas fa-trash'));

    divmethod.append(methodinput);
    divgrp.append(delbtn);
    divmethod.append(divgrp);
    
    $("#listMethods").append(divmethod);
}

//btn eliminar objeto html
function deleteInput(objectID){
    $("#attr-iri").remove();
    $("#"+objectID.id).remove();
}

//cambia los atributos de una clase
function changeAttributes(){
    var i = 0;
    var ats = [];
    var atobjs = [];
    var childs = ($("#listAttributes")).children();
    for (var i = 0; i <= childs.length - 1; i++) {
        ats[i]=$(childs[i]).find('.attributeClass').val()
        // ats[i]=$(childs[i]).children()[0].value;
        atobjs[i] = $(childs[i]).find('.attr-iri').val()
        childs[i].remove();
    }
    actualClass.model.attributes.attributes = ats;
    actualClass.model.attr('data-attr-iris', atobjs);
}

//cambia los metodos de una clase
function changeMethods(){
    var i = 0;
    var mts = [];
    var childs = ($("#listMethods")).children();
    for (var i = 0; i <= childs.length - 1; i++) {
        mts.push($(childs[i]).children()[0].value);
        childs[i].remove();
    }
    actualClass.model.attributes.methods = mts;
}

//cambia el nombre de una clase
function changeNameClass(){
    if (actualClass!=null){

        actualClass.model.attr('data-class-url', $('#class-url').val());
        
        if ($('#classRenameInput').val()!='undefined'){
            actualClass.model.attributes.name = $('#classRenameInput').val();
        }
    }
}

//cambia el tamaño de la clase segun el contenido
function resizeClass(cls){
    var w = 0;
    var ats = cls.attributes.attributes;
    var mts = cls.attributes.methods;
    //busca la palabra mas larga entre los atributos
    for (var i = ats.length - 1; i >= 0; i--) {
        if (ats[i].length>w){
            w = ats[i].length;
        }
    } //busca la palabra mas larga entre los atributos y los metodos
    for (var i = mts.length - 1; i >= 0; i--) {
        if (mts[i].length>w){
            w = mts[i].length;
        }
    }
    //compara el tamaño del nombre de la clase con el w. la clase vale +20 porque está centrada
    if (cls.attributes.name.length+20>w){
        w = cls.attributes.name.length;
    }

    //como minimo tiene que tener 100px;
    if (w*7<100){
        w = 15;
    }
    var h = ats.length+mts.length+10;
    cls.resize(w*7,h*8);
    //lanza un evento para que actualice los datos del objeto visual class.
    cls.trigger('change:name');
}

//cambia el tamaño de la clase para ocultar sus metodos y atributos
function compactClass(){
    if (!actualElement.model.attr('customAttr/isCompact')){
        var w = actualElement.model.attributes.name.length;

        //como minimo tiene que tener 100px;
        if (w*7<100){
            w = 15;
        }
        actualElement.model.attr('.uml-class-methods-text/text','');
        actualElement.model.attr('.uml-class-attrs-text/text','');
        actualElement.model.attr('.uml-class-methods-rect').height = 0;
        actualElement.model.attr('.uml-class-attrs-rect').height  = 0;
        actualElement.model.attr('.uml-class-name-rect').height = 40;
        //actualElement.model.resize(w*7,75);
        actualElement.model.attr('customAttr/isCompact',true);
        tools.css({
            height: 50
        });
    }else{
        //resizeClass(actualElement);
        actualElement.model.trigger('change:name');
        actualElement.model.attr('customAttr/isCompact',false);
        var h = actualElement.model.attr('.uml-class-attrs-rect').height + actualElement.model.attr('.uml-class-methods-rect').height + actualElement.model.attr('.uml-class-name-rect').height;
        tools.css({
            height: h
        });
    }
}



//confirma todos los cambios a la class. (desde interfaz con boton done).
function confirmMenuClass(){
    changeNameClass();
    changeAttributes();
    changeMethods();
    resizeClass(actualClass.model);
    //vuelve a su tamaño por lo que ya no esta compacta.
    actualClass.model.attr('customAttr/isCompact',false);
    actualClass = null;
    $("#menuClass").css({display:"none"});
}

//cancela los cambios a una clase.
function cancelMenuClass(){
    var i = 0;
    ($("#listAttributes")).empty();
    actualClass = null;
    $("#menuClass").css({display:"none"});
}
//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////


//////////////////////////////////////////////////
/////////////////// MENU PARA LAS GENERALIZACIONES
//////////////////////////////////////////////////
actualGeneralization = null;
function menuGeneralization(cellView){
    hideElementTools();
    actualGeneralization = cellView;
    //setear el valor que tiene la generalizacion en el input select
    $("#selectGeneralizationType").val(actualGeneralization.model.attr('text/text'));
    $("#menuGeneralization").css({display:"block"});
}

function confirmMenuGeneralization(){
    actualGeneralization.model.attr('text/text',$("#selectGeneralizationType").val());
    actualGeneralization.model.attr('customAttr/type',$("#selectGeneralizationType").val());
    actualGeneralization = null;
    $("#menuGeneralization").css({display:"none"});
}

function cancelMenuGeneralization(){
    actualGeneralization = null;
    $("#menuGeneralization").css({display:"none"});
}
//////////////////////////////////////
//////////////////////////////////////
//////////////////////////////////////
